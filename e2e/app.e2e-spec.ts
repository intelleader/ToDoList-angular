import { ToDoListAungularPage } from './app.po';

describe('to-do-list-aungular App', () => {
  let page: ToDoListAungularPage;

  beforeEach(() => {
    page = new ToDoListAungularPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
